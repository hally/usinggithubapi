package com.hally.samplegithubapi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hally.samplegithubapi.data.Owner;
import com.hally.samplegithubapi.task.DownloadImageTask;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment
{
	private ImageView _avatarImageView;
	private TextView _ownerRepoName;
	private TextView _profileLink;
	private Owner _owner;

	public DetailFragment()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		_owner = (Owner) getActivity().getIntent().getSerializableExtra(Owner.SER_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
		_avatarImageView = (ImageView) rootView.findViewById(R.id.avatar_icon);
		_ownerRepoName = (TextView) rootView.findViewById(R.id.owner_name);
		_profileLink = (TextView) rootView.findViewById(R.id.profile_link);

		_ownerRepoName.setText(_owner.getLogin());
		_profileLink.setText(_owner.getProfileUrl());
		DownloadImageTask downloadImageTask = new DownloadImageTask(_avatarImageView);
		downloadImageTask.execute(_owner.getAvatarUrl());

		return rootView;
	}
}
