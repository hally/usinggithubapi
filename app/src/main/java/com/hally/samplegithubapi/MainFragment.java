package com.hally.samplegithubapi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hally.samplegithubapi.data.Owner;
import com.hally.samplegithubapi.task.FetchDataTask;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment
{
	private ListView _listViewRepositories;
	private ArrayAdapter<String> _repotAdapter;
	private FetchDataTask _fetchDataTask;

	private AdapterView.OnItemClickListener onRepoItemClickListener =
			new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					Owner owner = _fetchDataTask.getOwnerInfo(position);
					Intent launchDetailActivity = new Intent(getActivity(), DetailActivity.class);

					Bundle bundle = new Bundle();
					bundle.putSerializable(Owner.SER_KEY, owner);

					launchDetailActivity.putExtras(bundle);

					startActivity(launchDetailActivity);
				}
			};

	public MainFragment()
	{
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		_repotAdapter = new ArrayAdapter<String>(getActivity(),
				R.layout.list_item_repository, R.id.list_item_reponame,
				new ArrayList<String>());

		View rootView = inflater.inflate(R.layout.fragment_main, container, false);

		_listViewRepositories = (ListView) rootView.findViewById(R.id
				.listview_repositories);
		_listViewRepositories.setAdapter(_repotAdapter);
		_listViewRepositories.setOnItemClickListener(onRepoItemClickListener);

		return rootView;
	}

	private void updateWeather()
	{
		_fetchDataTask = new FetchDataTask(this);
		_fetchDataTask.execute();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		updateWeather();
	}

	public ArrayAdapter<String> getRepoAdapter()
	{
		return _repotAdapter;
	}
}
