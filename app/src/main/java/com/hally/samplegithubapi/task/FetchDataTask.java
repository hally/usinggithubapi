package com.hally.samplegithubapi.task;

import android.net.Uri;
import android.os.AsyncTask;

import com.hally.samplegithubapi.MainFragment;
import com.hally.samplegithubapi.data.Owner;
import com.hally.samplegithubapi.util.FormatUtil;
import com.hally.samplegithubapi.util.TraceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Kateryna on 10.08.2015.
 */
public class FetchDataTask extends AsyncTask<String, Void, String[]>
{
	private final String CLASS_NAME = FetchDataTask.class.getSimpleName();
	private static final int NUMBER_DAYS = 7;
	private MainFragment _mainFragment;
	private ArrayList _repoOwnerInfo = new ArrayList();

	public FetchDataTask(MainFragment fragment)
	{
		_mainFragment = fragment;
	}

	public Owner getOwnerInfo(int position)
	{
		return (Owner)_repoOwnerInfo.get(position);
	}

	@Override
	protected String[] doInBackground(String... params)
	{
		TraceUtil.logD(CLASS_NAME, "onPerformSync", "Called.");

		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;

		// Will contain the raw JSON response as a string.
		String repoJsonStr = null;

		// %3A%3E = :>
		String dateQuery = "created:>"+FormatUtil.getDate(NUMBER_DAYS);
		String sortCriteria = "stars";
		String orderParam = "desc";

		try
		{
			final String BASE_URL =
					"https://api.github.com/search/repositories?";

			//https://api.github.com/search/repositories?q=created:>2015-08-04&sort=stars&order=desc
			final String QUERY_PARAM = "q";
			final String SORT_PARAM = "sort";
			final String ORDER_PARAM = "order";

			Uri builtUri = Uri.parse(BASE_URL).buildUpon()
					.appendQueryParameter(QUERY_PARAM, dateQuery)
					.appendQueryParameter(SORT_PARAM, sortCriteria)
					.appendQueryParameter(ORDER_PARAM, orderParam)
					.build();

			URL url = new URL(builtUri.toString());

			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();

			// Read the input stream into a String
			InputStream inputStream = urlConnection.getInputStream();
			StringBuffer buffer = new StringBuffer();
			if (inputStream == null)
			{
				// Nothing to do.
				return null;
			}
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String line;
			while ((line = reader.readLine()) != null)
			{
				// Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
				// But it does make debugging a *lot* easier if you print out the completed
				// buffer for debugging.
				buffer.append(line + "\n");
			}

			if (buffer.length() == 0)
			{
				// Stream was empty.  No point in parsing.
				return null;
			}

			repoJsonStr = buffer.toString();
			TraceUtil.logD(CLASS_NAME, "doInBackground", "repoJsonStr= " + repoJsonStr);
		}
		catch (IOException e)
		{
			TraceUtil.logE(CLASS_NAME, "doInBackground", "Error ", e);
			// If the code didn't successfully get the data, there's no point in attempting
			// to parse it.
			return null;
		}
		finally
		{
			if (urlConnection != null)
			{
				urlConnection.disconnect();
			}
			if (reader != null)
			{
				try
				{
					reader.close();
				}
				catch (final IOException e)
				{
					TraceUtil.logE(CLASS_NAME, "doInBackground", "Error closing stream", e);
				}
			}
		}

		try
		{
			return getRepoNameDataFromJson(repoJsonStr);
		}
		catch (JSONException e)
		{
			TraceUtil.logE(CLASS_NAME, "doInBackground", e.getMessage(), e);
			e.printStackTrace();
		}

		// This will only happen if there was an error getting or parsing the data.
		return null;
	}

	private String[] getRepoNameDataFromJson(String repoJsonStr)
			throws JSONException
	{
		final String JO_ITEMS = "items";
		final String JO_NAME = "name";

		final String JO_OWNER = "owner";

		final String JO_OWNER_LOGIN = "login";
		final String JO_OWNER_AVATAR_URL = "avatar_url";
		final String JO_OWNER_URL = "url";

		JSONObject repoJson = new JSONObject(repoJsonStr);
		JSONArray repoArray = repoJson.getJSONArray(JO_ITEMS);

		ArrayList<String> arrayList = new ArrayList<String>();

		for (int i = 0; i < repoArray.length(); i++)
		{
			JSONObject jsonObject = repoArray.getJSONObject(i);
			arrayList.add(i, jsonObject.getString(JO_NAME));

			// save owner information for details view
			JSONObject ownerObject = jsonObject.getJSONObject(JO_OWNER);
			Owner owner = new Owner();
			owner.setLogin(ownerObject.getString(JO_OWNER_LOGIN));
			owner.setAvatarUrl(ownerObject.getString(JO_OWNER_AVATAR_URL));
			owner.setProfileUrl(ownerObject.getString(JO_OWNER_URL));

			_repoOwnerInfo.add(i, owner);
		}

		String[] resultStrs = arrayList.toArray(new String[arrayList.size()]);

		for (String s : resultStrs)
		{
			TraceUtil.logV(CLASS_NAME, "getRepoNameDataFromJson", "Data entry: " + s);
		}
		return resultStrs;
	}

	@Override
	protected void onPostExecute(String[] result)
	{
		super.onPostExecute(result);

		if(result != null)
		{
			_mainFragment.getRepoAdapter().clear();
			_mainFragment.getRepoAdapter().addAll(result); // for HoneyComb and above
		}
	}
}
