package com.hally.samplegithubapi.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.hally.samplegithubapi.util.TraceUtil;

import java.io.InputStream;

/**
 * Created by Kateryna on 11.08.2015.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>
{
	private final String CLASS_NAME = FetchDataTask.class.getSimpleName();
	private ImageView _avatarImageView;

	public DownloadImageTask(ImageView imageView)
	{
		_avatarImageView = imageView;
	}

	@Override
	protected Bitmap doInBackground(String... params)
	{
		String url = params[0];

		Bitmap bitmap = null;

		try
		{
			InputStream inputStream = new java.net.URL(url).openStream();
			bitmap = BitmapFactory.decodeStream(inputStream);
		}
		catch (Exception e)
		{
			TraceUtil.logE(CLASS_NAME, "doInBackground", e.getMessage());
			e.printStackTrace();
		}

		return bitmap;
	}

	protected void onPostExecute(Bitmap result)
	{
		if (result != null)
		{
			_avatarImageView.setImageBitmap(result);
		}
	}
}
