package com.hally.samplegithubapi.data;

import java.io.Serializable;

/**
 * Created by Kateryna on 11.08.2015.
 */
public class Owner implements Serializable
{
	public  final static String SER_KEY = "com.hally.samplegithubapi.objectPass.ser";
	public String _login = "";
	public String _avatarUrl = ""; //"avatar_url"
	public String _profileUrl = ""; //"url"

	public void setLogin(String login)
	{
		_login = login;
	}

	public void setAvatarUrl(String avatarUrl)
	{
		_avatarUrl = avatarUrl;
	}

	public void setProfileUrl(String profileUrl)
	{
		_profileUrl = profileUrl;
	}

	public String getLogin()
	{
		return _login;
	}

	public String getAvatarUrl()
	{
		return _avatarUrl;
	}

	public String getProfileUrl()
	{
		return _profileUrl;
	}
}
