package com.hally.samplegithubapi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Kateryna on 10.08.2015.
 */
public class FormatUtil
{
	public static String getDate(int daysNumber)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -daysNumber);

		Date todate = cal.getTime();
		String fromdate = dateFormat.format(todate);
		return fromdate;
	}
}
