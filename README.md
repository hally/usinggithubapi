# README #


Sample project "Github API":
Uses the Github Rest API (https://developer.github.com/v3/) to display a list of the trending Github repositories for the last week.
When one of the repository is selected in the list, displays its owner information (avatar, name, profile url) and top contributors list with name, avatar and profile url. Minimum supported API level: 14

**Nexus 5 Screenshots**

![Screenshot_2015-08-11-11-35-15_.png](https://bitbucket.org/repo/Kqdg4g/images/1635889244-Screenshot_2015-08-11-11-35-15_.png)

![Screenshot_2015-08-11-16-26-30.png](https://bitbucket.org/repo/Kqdg4g/images/731307768-Screenshot_2015-08-11-16-26-30.png)
